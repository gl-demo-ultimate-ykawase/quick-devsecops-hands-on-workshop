# Lab 1 - 開始為 Pipeline 加上掃描
在 CI/CD Pipeline 中加上 SAST、IaC Scanning、Secret Detection。

## 目錄
[[_TOC_]]

## 操作步驟流程
```mermaid
graph TB
    subgraph "計劃"
        Issue_board[訪問Issue board] --> 建立Issue
    end
    subgraph "Merge Request"
        subgraph "同時新建MR和branch"
            建立Issue --> MR[建立Merge Request]
            建立Issue --> fb[建立feature branch]
        end
        MR[建立Merge Request] --> 修改.gitlab-ci.yml
        fb[建立feature branch] --> 修改.gitlab-ci.yml
        修改.gitlab-ci.yml --> CI/CD_Pipeline_in_MR[CI/CD Pipeline]
        CI/CD_Pipeline_in_MR[CI/CD Pipeline] --> 確認追加的安全掃描job
    end
```
## Plan：運用 Issue 開始你的 DevSecOps
為了讓專案管理物件（Epic、Issue 等）和實際的開發工作之間能彼此建立連結，維持專案追蹤性，GitLab 建議使用者透過 Issue 建立 Merge request。

在這場 Hand-oh Lab 中，我們會透過 **Issue boards（看板）** 建立 Issue。

### 操作步驟
1. 進入你在 Lab 0 順利 Import 的 [Quick Devsecops Hands On Workshop] Project（此 Project 會在你的「My Test Group」中）。
1. 在 Project 頁的左側 Sidebar，點擊 `[Plan]` > `[Issue boards]`。

   ![](../img/lab1/001.png)

1. 在 Issue boards 的 `[Open]` 列表，按下 `[+]` 按鈕，`[Title]` 欄位輸入 `Start DevSecOps` ，按下 `[Create issue]`。

   ![](../img/lab1/002.png)

1. 當 Issue 被順利建立後，點擊 Issue: `[Start DevSecOps]` 的連結，進入該 Issue 的詳細內容頁面。

## 建立 MR 和 Branch
從 Issue 建立 Merge request 時，GitLab 會把新的 Branch 也一併建立。這樣每個 Feature branch 都會由 Merge request 管理，避免產生未被 MR 管理的大量 Feature branch。

另外，在建立 MR 時，可以指派 Assignees 與 Reviewers，確實分配要由誰來負責追蹤此 MR，以及誰來負責 Review。（在本 Lab 中，我們要一人分飾兩角，自己扮演開發者及 Reviewer。）

### 操作步驟
1. 在 Issue: `[Start DevSecOps]` 頁面中，按下 `[Create merge request]` 按鈕。

   ![](../img/lab1/003.png)

1. 在 `[New merge request]` 頁面中，將 `[Assignees]` 與 `Reviewers` 都設置為你自己。最後按下按下頁底附近的 `[Create merge request]` 按鈕。

   ![](../img/lab1/004.png)

1. 在 MR: `[Draft: Resolve "Start DevSecOps"]` 頁面中，確認 MR 有同時新建立了 Branch: `[1-start-devsecops]`。

   ![](../img/lab1/005.png)

1. 在 `[Draft: Resolve "Start DevSecOps"]` 頁面中，會自動帶有原 Issue 的連結（例如 `[Closes #1]`）。（GitLab 可以在 User 沒有修改任何原始碼的狀況下，就自動將 Issue 和 MR 彼此關聯，促進團隊在 MR 之間進行良好的協作。）

   ![](../img/lab1/006.png)

## 修改 `.gitlab-ci.yml`
這一次的 `.gitlab-ci.yml` 修改，我們會將 **SAST**、**IaC Scanning**、**Secret Detection** 這三種 Scan job 放進我們的 CI/CD Pipeline。

### 操作步驟

1. 在 MR: `[Draft: Resolve "Start DevSecOps"]` 頁面中，按下右上角的 `[Code]` 按鈕，接著選擇下拉選單的 `[Open in Web IDE]`，藉此開啟 GitLab 的 Web IDE。（瀏覽器會彈出新的 Tab。）（你也可以在 MR 頁面中，按下熱鍵 `.` 直接進入 Web IDE。）

   ![](../img/lab1/007.png)

1. 在 Web IDE 中，從左側的檔案清單中找到 `.gitalb-ci.yml` 檔案，點擊它，將檔案開啟。

   ![](../img/lab1/008.png)

1. 將下述的 CI Job 定義放進 `.gitalb-ci.yml` 檔案的最底部。
    ```yaml
      # 請將以下內容，放在 - template: Jobs/Build.gitlab-ci.yml 的下一行
      ## 貼上後，記得查看 YAML 的縮排是否正確

      - template: Security/SAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml
      - template: Security/SAST-IaC.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/SAST-IaC.gitlab-ci.yml
      - template: Security/Secret-Detection.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Secret-Detection.gitlab-ci.yml
    ```

   ![](../img/lab1/009.png)

1. 點擊左側選單的 `Source Control`（它上面應該會額外出現 `(1)` ）的圖示），並按下 `[Commit to ‘1-start-devsecops’]` 按鈕，將我們修改好的程式碼 Commit 到 Branch 中。

   ![](../img/lab1/010.png)

1. 接著要查看修改後的成果，請切換你的瀏覽器 Tab，回到原來的 Merge request 頁面。

## 確認剛才新增的安全掃描 Job

延續前面的操作，我們建立了 Issue、Feature Branch 與 MR，這三者已經被關聯在一起。因為我們有設置 `.gitlab-ci.yml`，因此當我們在 Feature branch 送出 Commit 時，GitLab 會自動執行 CI/CD Pipeline。我們可以透過 UI 確認 Pipeline 是否有正確執行安全掃描。

### 操作步驟

1. 在 MR 頁面中，可以找到以 `#` 為開頭的 CI/CD Pipeline 連結（例如 `#933345984`）。可透過連結，另開新的瀏覽器 Tab 瀏覽 Pipeline 頁面，等待並確認新增的 [kics-iac-sast]、[secret_detection]、[semgrep-sast] 三種安全掃描 Job 在 test stage 皆有順利執行。（整個 Pipeline 預計需花 2 分鐘左右執行。）

   ![](../img/lab1/011.png)

1. 等到 Pipeline 執行完之後，MR 會顯示 `[Security scanning detected N new potential vulnerabilities]`。點擊右側的箭頭圖標，確認這三種安全掃描 Job 所檢測出的漏洞。（或者，你也可以在 Pipeline 的頁面中，查看個別 Pipeline 所找到漏洞。）

   ![](../img/lab1/012.png)

   - 特別說明：Secret Detection 的預設動作是只會檢測該次 Commit 產生的 Secret。如果你要檢測的是 Repository / Project 內已存在的 Secret，需要等到將 MR 合併到 default branch（在此 Project 即是 main branch） 之後才會被掃描。


## 參考網址
- [Static Application Security Testing (SAST) | GitLab](https://docs.gitlab.com/ee/user/application_security/sast/)
- [Infrastructure as Code (IaC) Scanning | GitLab](https://docs.gitlab.com/ee/user/application_security/iac_scanning/)
- [Secret Detection | GitLab](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
- [Creating merge requests from an issue](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#from-an-issue)

---

## [上一個 Lab - Lab 0](../lab0/README.md)

## [下一個 Lab - Lab 2](../lab2/README.md)

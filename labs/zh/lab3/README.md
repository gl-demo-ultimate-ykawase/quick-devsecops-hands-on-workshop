# Lab 3 - 體驗如何修復 SAST 及 Dependency Scanning 等漏洞

## 目錄

[[_TOC_]]

在 Lab 3，我們會嘗試修復不同類型的漏洞。

## 操作步驟流程
下述的操作步驟表示在 Lab 3 中每一個修復漏洞的section的流程。
```mermaid
graph TB
    subgraph "Resolve Vulnerability"
        Vulnerability_Report[檢核 Vulnerability Report] --> Confirm_Vuln_Detail[確認漏洞細節]
        Confirm_Vuln_Detail[確認漏洞細節] --> Create_Issue
        Create_Issue[針對漏洞建立 Issue] --> Create_MR_and_FB
        Create_MR_and_FB[建立 Merge request 和 Feature branch] --> Resolve_vulnerability
        Resolve_vulnerability[修復漏洞] --> Mark_as_Ready
        Mark_as_Ready[Mark as Ready] --> Approve
        Approve --> Merge
        Merge --> Reconfirm_Vulnerability_Report[確認 Vulnerability Report]
    end
```

## SAST - Fix SQL Injection

### 確認漏洞細節
1. 點選左側 Sidebar 的 `[Secure]` > `[Vulnerability report]`，查看 `[Vulnerability report]` 頁面。

1. 利用 UI 上的 Filter，快速尋找 SAST 的漏洞。
   - [Severity]: 選擇 Medium
   - [Tool]: 選擇 SAST

   ![](../img/lab3/001.png)


1. 找到 `[Improper Neutralization of Special Elements used in an SQL Command ('SQL Injection') notes/db.py:39]`，點選 `[Description]` 列的連結（漏洞標題的連結）。

   ![](../img/lab3/002.png)

1. 在個別的漏洞頁面，確認漏洞內容。

### 針對漏洞建立 Issue、Merge request 以及 Feature Branch
- 在個別的漏洞頁面，點選底部的 `[Create issue]` 按鈕。

   ![](../img/lab3/003.png)


- 進到建立 issue 頁面後，
    1. 在 Issue 的 [Description] 欄底部，移除預設的 [`/confidential`] 內容。
    1. 取消勾選 [This issue is confidential and should only be visible to team members with at least Reporter access.]。
    1. Assignees 選擇 [Assign to me]。

    ![](../img/lab3/004.png)

    4. 點選 [Create issue]。

- 到 Issues 選擇剛才建立的 Issue，點選 `[Create merge request]`。
    - 特別說明： 如介面上沒有 [Create merge request] 按鈕，可能是右方 Confidentiality 設定為 on，此時只需要改設定 `[Turn Off]`，完成後重新整理介面即可看到。
    ![](../img/lab3/005.png)

- 確認 New merge request 內容後，點選 `[Create merge request]`。


### 漏洞修復
- 進入到 Merge requests 選擇剛才建立的 Merge request，於右上角 `[Code -> Open in Web IDE]` 開始進行漏洞修復。
    1. 選擇 `[notes/db.py]` 檔案，並移動至 36 行，修改原始碼如下：
    ```python
    - query = "DELETE FROM notes WHERE id='%s'" % id
    + query = "DELETE FROM notes WHERE id='%(id)s'"
    ```
    1. 進入至 `Source Control`，輸入 Commit message，如 `Fix SQL Injection`，並選擇 [Commit to `1-invest....`]，建立對應 commit。
    1. 回到 Merge request 頁面，確認 Pipeline 執行完成並解決此問題。
    1. 點選 `[Mark as ready]` 按鈕。（如果在 Lab 0 設定過 Merge request 的 Approval rule，接著點選 `[Approve]` 按鈕。）
    1. 點選 `[Merge]` 按鈕。
- 回到 Vulnerability report 標註問題已解決。

  ![](../img/lab3/006.png)


## Dependency Scanning - Fix Unintended leak of Proxy-Authorization header in requests

- 點選左側 Sidebar 的 `[Secure]` > `[Vulnerability report]`，查看 `[Vulnerability report]` 頁面。

- 利用 UI 上的 Filter，快速尋找 Dependency Scanning 的漏洞。
   - [Tool]: 選擇 Dependency Scanning

- 找到 `[Unintended leak of Proxy-Authorization header in requests]`，點選 `[Description]` 列的連結（漏洞標題的連結）。
- 建立 Issue 並建立 Merge request 後，點選 Web IDE 開始進行弱點修復。（請參照：[SAST - Fix SQL Injection 的「針對漏洞建立 Issue、Merge request 以及 Feature Branch」](#針對漏洞建立-issuemerge-request-以及-feature-branch)）
- 進行漏洞修復。
    1. 選擇 `[requirements.txt]` 檔案，並調整第 7 行，修改原始碼如下：
    ```ini
    - requests == 2.30.0
    + requests == 2.31.0
    ```
    1. 進入 `Source Control`，輸入 Commit message，如 `Fix Unintended leak of Proxy-Authorization header`，建立 Commit。
    1. 回到 Merge request 頁面，確認 Pipeline 執行完成，並解決此問題。
    1. 點選 `[Mark as ready]` 按鈕。(如果在 Lab 0 設定過 Merge request 的 Approval rule，接著點選 `[Approve]` 按鈕。)
    1. 點選 `[Merge]` 按鈕。
- 回到 Vulnerability report 標註問題已解決。

## Container Scanning - Fix CVE-2019-8457 and CVE-2019-14697

- 點選左側 Sidebar 的 `[Secure]` > `[Vulnerability report]`，查看 `[Vulnerability report]` 頁面。

- 利用 UI 上的 Filter，快速尋找 Container Scanning 的漏洞。
   - [Severity]: 選擇 Critical
   - [Tool]: 選擇 Container Scanning

- 找到 `[CVE-2019-8457]` 問題，建立 Issue。
- 找到 `[CVE-2019-14697]` 並新增至剛建立的 Issue。
- 回到建立的 Issue 建立 Merge request，點選 Web IDE 開始進行弱點修復。（請參照：[SAST - Fix SQL Injection 的「針對漏洞建立 Issue、Merge request 以及 Feature Branch」](#針對漏洞建立-issuemerge-request-以及-feature-branch)）
- 進行漏洞修復。
    1. 選擇 [Dockerfile] 檔案，並如下調整原始碼：
    ```dockerfile
    - FROM python:alpine3.8
    + FROM python:alpine3.9

    RUN apk update
    + RUN apk upgrade
    ...
    ```
    1. 進入 `Source Control`，輸入 Commit message，如 `Fix CVE-2019-8457 and CVE-2019-14697`，建立 Commit。
    1. 回到 Merge request 頁面，確認 Pipeline 執行完成，並解決此問題。
    1. 點選 `[Mark as ready]` 按鈕。(如果在 Lab 0 設定過 Merge request 的 Approval rule，接著點選 `[Approve]` 按鈕。)
    1. 點選 `[Merge]` 按鈕。
- 回到 Vulnerability report ，會發現此次的調整除改善原定的 `CVE-2019-8457 ` 與 `CVE-2019-14697` 外，亦解決其他非預期的問題，同樣標註問題已解決。

## IaC Scanning - Missing User Instruction

1. 點選左側 Sidebar 的 `[Secure]` > `[Vulnerability report]`，查看 `[Vulnerability report]` 頁面。

- 利用 UI 上的 Filter，快速尋找 IaC Scanning 的漏洞。
   - [Severity]: 選擇 Critical
   - [Tool]: 選擇 SAST

- 找到 `[A user should be specified in the dockerfile, otherwise the image will run as root]`，點選 `[Description]` 列的連結（漏洞標題的連結）。
- 建立 Issue 並建立 Merge request 後，點選 Web IDE 開始進行弱點修復。（請參照：[SAST - Fix SQL Injection 的「針對漏洞建立 Issue、Merge request 以及 Feature Branch」](#針對漏洞建立-issuemerge-request-以及-feature-branch)）
- 進行漏洞修復。
    1. 選擇 `[Dockerfile]` 檔案，並如下調整原始碼：
    ```dockerfile
    ...
    RUN pip install -r requirements.txt
    COPY . /app

    + RUN adduser --system --no-create-home nonroot
    + USER nonroot

    ENTRYPOINT [ "python" ]
    ...
    ```
    
    1. 進入 `Source Control`，輸入 Commit message，如 `fix The 'Dockerfile' does not contain any 'USER' instruction`，建立 Commit。
    1. 回到 Merge request 頁面，確認 Pipeline 執行完成，並解決此問題。
    1. 點選 `[Mark as ready]` 按鈕。(如果在 Lab 0 設定過 Merge request 的 Approval rule，接著點選 `[Approve]` 按鈕。)
    1. 點選 `[Merge]` 按鈕。
- 回到 Vulnerability report 標註問題已解決。

## Secret Detection - Fix GitLab Personal Access Token 

### 既有漏洞修復

- 點選左側 Sidebar 的 `[Secure]` > `[Vulnerability report]`，查看 `[Vulnerability report]` 頁面。

- 利用 UI 上的 Filter，快速尋找 Secret Detection 的漏洞。
   - [Severity]: 選擇 Critical
   - [Tool]: 選擇 Secret Detection

- 找到 `[GitLab Personal Access Token]`，點選 `[Description]` 列的連結（漏洞標題的連結）。
- 建立 Issue 並建立 Merge request 後，點選 Web IDE 開始進行弱點修復。（請參照：[SAST - Fix SQL Injection 的「針對漏洞建立 Issue、Merge request 以及 Feature Branch」](#針對漏洞建立-issuemerge-request-以及-feature-branch)）
- 進行漏洞修復。
    1. 選擇 `[.env.sample]` 檔案，並如下調整原始碼：
    ```ini
    - GITLAB_ACCESS_TOKEN="glpat-Bt3b82ag-jEvZtgKuMeq"
    + GITLAB_ACCESS_TOKEN=""
    ```
    
    1. 進入 `Source Control`，輸入 Commit message，如 `Fix GitLab Personal Access Token`，建立 Commit。
    1. 回到 Merge request 頁面，確認 Pipeline 執行完成，並解決此問題。
    1. 點選 `[Mark as ready]` 按鈕。(如果在 Lab 0 設定過 Merge request 的 Approval rule，接著點選 `[Approve]` 按鈕。)
    1. 點選 `[Merge]` 按鈕。
- 回到 Vulnerability report 確認 `[GitLab Personal Access Token]` 已解決，並正式標註問題解決。

### 模擬開發過程中發生機敏資料洩漏

- 點選左側 Sidebar 的 `[Plan]` > `[Issues]`，點選 `[New issue]`，標題如：`Simulate secret detect`，建立新 Issue，並建立 Merge request，在建立完成後點選 Web IDE 開始進行模擬。
- 模擬 Secret Detection
   1. 模擬在開發過程中，將特定環境的環境變數檔案存入儲存庫(Git Repo)
   1. 於左側 EXPLORER 點選右鍵後，選擇 `[New File...]`，輸入檔名，如：`.env.prod`
   1. 於 `.env.prod` 內容中加入 `GITLAB_ACCESS_TOKEN="glpat-Bt3b82ag-jEvZtgKuMeq"`
   1. 進入 `Source Control`，輸入 Commit message，如 `Simulate secret detection`，建立 Commit。
   1. 回到 Merge request 頁面，當 Pipeline 執行完成後，可於 `Security scanning` 查看 `Full report`。(如未出現 Security scanning 可重新整理頁面)
   1. 可以看到於 Secret detection 中偵測到剛加入的模擬內容判斷為 `Critical GitLab Personal Access Token`。
   1. 回到 Web IDE，重新編輯 `.env.prod`，如上一小節清除機敏資料後，重新 commit，待 Pipeline 執行完成後，可發現問題已排除(不會再偵測到 Secret detection)。

## [上一個 Lab - Lab 2](../lab2/README.md)
<!-- ## [下一個Lab - Lab 4](../lab4/README.md) -->

# Lab 2 - 為 Pipeline 加上更多掃描
在 CI/CD Pipeline 中加上 Dependency Scanning、Container Scanning。

## 目錄
[[_TOC_]]

## 操作步驟流程
```mermaid
graph TB
    subgraph "Merge Request"
        修改.gitlab-ci.yml --> CI/CD_Pipeline_in_MR[CI/CD Pipeline]
        CI/CD_Pipeline_in_MR[CI/CD Pipeline] --> 確認追加的安全掃描job
        確認追加的安全掃描job --> Mark_as_Ready[Mark as Ready]
        Mark_as_Ready[Mark as Ready] --> Approve
        Approve --> Merge
    end
    subgraph "Main Branch"
        Merge --> CI/CD_Pipeline_in_main[CI/CD Pipeline]
        CI/CD_Pipeline_in_main[CI/CD Pipeline] --> Vulnerability_Report[確認Vulnerability Report]
        Vulnerability_Report[確認Vulnerability Report] --> 確認SBOM
    end
```

## 修改 `.gitlab-ci.yml`
這一次修改 `.gitlab-ci.yml`，會替 CI/CD Pipeline 加上 Dependency Scanning、Container Scanning 的 Job。

### 操作步驟
1. 在 MR: `[Draft: Resolve "Start DevSecOps"]` 頁面中，按下右上角的 `[Code]` 按鈕，接著選擇下拉選單的 `[Open in Web IDE]`，藉此開啟 GitLab 的 Web IDE。（通常瀏覽器會彈出新的 Tab。）（你也可以在 MR 頁面中，按下熱鍵 `.` 直接進入 Web IDE。）

   ![](../img/lab1/007.png)


1. 在 Web IDE 中，從左側的檔案清單中找到 `.gitalb-ci.yml` 檔案，點擊它，將檔案開啟。

   ![](../img/lab1/008.png)

1. 將下述的 CI Job 定義放進 `.gitalb-ci.yml` 檔案的最底部。
    ```yaml
      # 請將以下內容，放在 - template: Security/Secret-Detection.gitlab-ci.yml 的下一行
      ## 貼上後，記得查看 YAML 的縮排是否正確

      - template: Security/Dependency-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml
      - template: Security/Container-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
    ```

   ![](../img/lab2/001.png)


1. 點擊左側選單的 `Source Control`（它上面應該會額外出現 `(1）` 的圖示），並按下 `[Commit to ‘1-start-devsecops’]` 按鈕，將我們修改好的程式碼 Commit 到 Branch 中。

   ![](../img/lab1/010.png)

1. 接著要查看修改後的成果，請切換你的瀏覽器 Tab，回到原來的 Merge request 頁面。

## 確認剛才再次追加的安全掃描 Job
1. 在 MR 頁面中，可以找到以 `#` 為開頭的 CI/CD Pipeline 連結（例如 `#933345984`）。可透過連結，另外開啟新的瀏覽器 Tab 瀏覽 Pipeline 頁面，等待並確認新增的 [gemnasium-python-dependency_scanning]、[container_scanning] 兩種安全掃描 Job 在 test stage 皆有順利執行。（整個 Pipeline 預計需花 3 分鐘左右執行。）

   ![](../img/lab1/011.png)

1. 等到 Pipeline 執行完之後，MR 會顯示 `[Security scanning detected N new potential vulnerabilities]`。點擊右側的箭頭圖示，確認新增加的兩種安全掃描 Job 所檢測出的漏洞。（或者，你也可以在 Pipeline 的頁面中，查看個別 Pipeline 所找到漏洞。）

   ![](../img/lab2/002.png)

## 將 Merge request 的狀態改為 Mark as Ready
Merge request 在剛建立時，會是 `Draft:` 的狀態，代表開發者還在解決 Issue，目前尚不能將程式碼合併；而 [Mark as Ready](https://docs.gitlab.com/ee/user/project/merge_requests/drafts.html#mark-merge-requests-as-ready) 則表示該 MR 的開發者認為 Issue 已處理完畢，可以將 MR 合併了。

### 操作步驟

1. 請假設你現在是開發者的身份，點擊 MR 頁面右上角的 `[︙]` > `[Mark as Ready]`，並確認 MR 標題的 `Draft:` 文字有被自動移除。

   ![](../img/lab2/003.png)

2. 作為開發者，接著你可以通知其他的 Reviewers 或 Approvers 進行 Code Review。

## Approve
特別說明：如果你沒有在 Lab 0 設定過 Merge request 的 Approval rule，可以跳過這一個步驟。

Merge request (MR) 是維持軟體品質的重要關卡，一般來說會建議每一個 MR 都需要經過 [approval](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html) 後，才可以進行合併。

### 操作步驟

1. 請假設你自己是負責 Review MR 的 Reviewer，在 review 完畢後，按下 `[Approve]` 按鈕。

## Merge
通過了 MR 的 Approval 步驟，實際該在何時合併 MR 就可以由相關的開發者（非 Maintainer / Owner）自行執行。

### 操作步驟
1. 請假設你現在是開發者，確認 MR 已通過 Approval 後，按下 `[Merge]` 按鈕。
   （特別說明：在真實專案的 MR 中，如果程式碼被檢測到了多個 Critical 的漏洞，其實開發者不應該將 MR 合併進去；批准者也不應該批准 MR。但在這場 Lab，我們為了讓 Lab 能繼續進行，會請學員執行 MR 合併。）
1. 合併 MR 之後，確認該 MR 有順利產生針對 main branch 執行的 CI/CD Pipeline。

   ![](../img/lab2/004.png)

## 確認 Vulnerability Report
Vulnerability Report 會顯示在 default branch 所檢測出的漏洞列表。這些即是在 default branch 中檢測到的漏洞，也正是整個 DevSecOps 週期的起點。

### 操作步驟
1. 等到 main brach 的 CI/CD Pipeline 執行完之後，點擊左側 sidebar 的 `[Secure]` > `[Vulnerability report]`。

   ![](../img/lab2/005.png)

1. 確認頂部附近的 `[Development vulnerabilities N]`，main branch 會檢測出大約有 21 個漏洞。

   ![](../img/lab2/006.png)

## 確認 SBOM
SBOM（Software Bill of Materials）是軟體使用的第三方軟體元件的清單。可以幫助企業瞭解其使用之軟體的全貌，以及該軟體的安全性、合規性和風險。採用著自動生成最新的 SBOM 機制，就可以對於重大漏洞快速做出反應。

### 操作步驟

1. 點擊左側 Sidebar 的 `[Secure]` > `[Dependecny list]`。

   ![](../img/lab2/007.png)

1. 確認由 Dependency Scanning 和 Container Scanning 生成的 SBOM。
1. 其中幾個第三方 Component 含有已知的漏洞，點開 `[Component]`列的箭頭圖標，確認漏洞內容。

   ![](../img/lab2/008.png)


## 參考網址
- [Dependency Scanning | GitLab](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- [Container Scanning | GitLab](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
- [Vulnerability Report | GitLab](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/)
- [Dependency list | GitLab](https://docs.gitlab.com/ee/user/application_security/dependency_list/)


## [上一個 Lab - Lab 1](../lab1/README.md)
## [下一個 Lab - Lab 3](../lab3/README.md)
# Lab 0 - 啟動你的 Training Environment

## 目錄
[[_TOC_]]

## 準備 GitLab.com 帳號
1. 本 Lab 需要使用 GitLab.com 帳號，如果你沒有帳號，請從 https://gitlab.com/users/sign_up 新建一個帳號之後，登入 GitLab.com。

## 新建用於 Hands-on Labs 的 GitLab Group
此 hands-on Lab 將使用 GitLab.com 上的 Training Environment。請依照以下步驟，取得你自己專用的 Training Environment - 「My Test Group」（它是一個特殊的 GitLab Group）。

最終你所建立的 Training Environment - 「My Test Group」 的 URL 會類似下面的範例：
```
# URL 規則：
https://gitlab.com/gitlab-learn-labs/environments/session-<SESSION_ID>/<RANDOM_CODE>

# 舉例：
https://gitlab.com/gitlab-learn-labs/environments/session-26828cef/iu1x0th0
```

### 操作步驟
1. 前往 GitLab Demo Portal ( https://gitlabdemo.com/invite )，申請建立「My Test Group」。
1. 點擊 `[Invitation Code]` 欄位，填入講師提供的 **invitaion code**。按下 `[Provision Tranining Enviroment]` 按鈕。

   ![](../img/lab0/001.png)

1. 點擊 `[GitLab.com Username]` 欄位，填入你在建立 GitLab.com 帳號時指定的 `username`，再次按下 `[Provision Tranining Enviroment]` 按鈕。（如不確定自己的 `username`，可以在 gitlab.com 點擊自己的頭像或進入 User Profile 中找到。）

   ![](../img/lab0/002.png)

1. 在 `[Your GitLab Credentials]` 頁面，將看到系統已順利幫你在 GitLab.com 上建立「My Test Group」，並提供 URL。

   ![](../img/lab0/003.png)


1. 點擊 `[GitLab URL]` 右側的 URL，確認可以成功連上自己的「My Test Group - <RANDOM_CODE>」。（在後續的 Lab 過程，學員都會在自己的「My Test Group」進行操作，建議你可以將此 URL 暫時加入我的最愛，或將 URL 筆記下來。）

   ![](../img/lab0/004.png)


## Import 此次 Hands-on Labs 的 Sample Project
在這次的 Hand-on Lab，我們準備了一份範例程式，它是一個以 Python 開發的 Notes 工具。這份範例程式中，已經內含了為本次 Lab 事先準備好的漏洞，請學員將這份範例程式 Import 至你的 Tranining Enviroment。
### 操作步驟
1. 進入你自己的「My Test Group - <RANDOM_CODE>」。
1. 按下 `[New project]` 按鈕。
1. 點擊 `[Import project]` 。

   ![](../img/lab0/005.png)


1. 在 `[Import project]` 頁面，找到並按下 `[Repository by URL]` 按鈕。

   ![](../img/lab0/006.png)


1. 點擊 `[Git repository URL]` 欄位，填入 `https://gitlab.com/gitlab-learn-labs/sample-projects/quick-devsecops-hands-on-workshop.git`。

   ![](../img/lab0/007.png)


1. 按下 `[Create project]` 按鈕。
1. 等到順利完成 Import project 之後，畫面上會顯示你個人自專用的 **[Quick DevSecOps Hands-on Workshop]** project 的主要頁面。（在後續的 Lab 過程，學員都會在自己的 Project 內進行操作，建議你可以將此 Project 之 URL 暫時加入我的最愛，或將 URL 筆記下來。）

   ![](../img/lab0/008.png)


## 調整 GitLab Project 的設定
### 設定 Merge Request Approval 規則
特別說明：此為 **Optional** 的設定，跳過這個設定也可以進行後續的 Lab。

Merge request (MR) 是維持軟體品質的重要關卡，一般來說會建議每一個 MR 都需要經過 [Approval](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html) 後，才可以進行合併。

### 操作步驟

1. 在自己專用的 Project: [Quick DevSecOps Hands-on Workshop]的主頁，點擊左側 `[Settings]` > `[Merge requests]`。

   ![](../img/lab0/009.png)


1. 在 Section: `[Merge request approvals]` 裡的 `[Approval rules]`，按下 `[Add approval rule]` 按鈕。

   ![](../img/lab0/010.png)


1. 依照下面說明的內容逐一輸入需要的資訊之後，按下 `[Add approval rule]` 按鈕，新增一項 Approval 規則。
   - **\[Rule name]**: `main`
   - **\[Target branch]**: `main`
   - **\[Approvals requreid]**: `1`
   - **\[Add approveers]**: 輸入自己的 Username 之後，UI 嘗試 Search 並跳出你的 User，接著選擇自己的 Username。

   ![](../img/lab0/011.png)


1. 在 Section: `[Merge request approvals]` 裡的 `[Approval settings]`，取消勾選 `[Prevent approval by author]`。（因為在此 Hand-on Lab 中，學員需要使用同一個 GitLab.com 帳號，模擬擔任開發者和批准者(Approver)兩種角色。）

   ![](../img/lab0/012.png)


1. 按下底部的 `[Save changes]` 按鈕。

   ![](../img/lab0/013.png)

<!-- 
### 停用Group Runner
在[Quick DevSecOps Hands-on Workshop] project，為了只用GitLab.com 的 SaaS Runner，需要在project設定停用在頂層 group 設置的 Group Runner。

1. 在自己專用的[Quick DevSecOps Hands-on Workshop]的主頁，點擊左側 `[Settings] > [CI/CD]`。
1. 在 `[CI/CD Settings]` 頁面，點擊 `[Runners]` 右側的 `[Collapse]` 按鈕，展開 Runners 設定內容。
1. 按下 `[Group runners]` 下的 `[Disable group runners]` 按鈕，停用 Group Runner。 

   ![](../img/lab0/014.png)

-->

## [下一個 Lab - Lab 1](../lab1/README.md)
